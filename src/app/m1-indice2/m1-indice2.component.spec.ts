import { ComponentFixture, TestBed } from '@angular/core/testing';

import { M1Indice2Component } from './m1-indice2.component';

describe('M1Indice2Component', () => {
  let component: M1Indice2Component;
  let fixture: ComponentFixture<M1Indice2Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [M1Indice2Component]
    });
    fixture = TestBed.createComponent(M1Indice2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
