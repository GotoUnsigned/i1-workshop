import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MdbCollapseModule } from 'mdb-angular-ui-kit/collapse';
import { HeaderComponent } from './header/header.component';
import { HeaderGameComponent } from './header-game/header-game.component';
import { LoreGameComponent } from './lore-game/lore-game.component';
import { M1Indice1Component } from './m1-indice1/m1-indice1.component';
import { M1Indice2Component } from './m1-indice2/m1-indice2.component';
import { Mission1MjComponent } from './mission1-mj/mission1-mj.component';
import { PlayerConnectionComponent } from './player-connection/player-connection.component';
import { TeamCreationComponent } from './team-creation/team-creation.component';

const routes: Routes = [
  { path: '', component: HeaderComponent },
  { path: 'headerGame', component: HeaderGameComponent },
  { path: 'LoreGame', component: LoreGameComponent },
  { path: 'M1Indice1', component: M1Indice1Component },
  { path: 'M1Indice2', component: M1Indice2Component },
  { path: 'Mission1MJ', component: Mission1MjComponent },
  { path: 'PlayerConnection', component: PlayerConnectionComponent },
  { path: 'TeamCreation', component: TeamCreationComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), MdbCollapseModule],

  exports: [RouterModule, MdbCollapseModule],
})
export class AppRoutingModule {}
