import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoreGameComponent } from './lore-game.component';

describe('LoreGameComponent', () => {
  let component: LoreGameComponent;
  let fixture: ComponentFixture<LoreGameComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoreGameComponent]
    });
    fixture = TestBed.createComponent(LoreGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
