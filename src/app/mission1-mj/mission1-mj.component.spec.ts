import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Mission1MjComponent } from './mission1-mj.component';

describe('Mission1MjComponent', () => {
  let component: Mission1MjComponent;
  let fixture: ComponentFixture<Mission1MjComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [Mission1MjComponent]
    });
    fixture = TestBed.createComponent(Mission1MjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
