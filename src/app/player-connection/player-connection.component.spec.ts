import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerConnectionComponent } from './player-connection.component';

describe('PlayerConnectionComponent', () => {
  let component: PlayerConnectionComponent;
  let fixture: ComponentFixture<PlayerConnectionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlayerConnectionComponent]
    });
    fixture = TestBed.createComponent(PlayerConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
